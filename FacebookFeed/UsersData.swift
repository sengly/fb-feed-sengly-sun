//
//  UsersData.swift
//  FacebookFeed
//
//  Created by Sengly Sun on 12/3/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation

struct UserData {
    var userName: String
    var userProfile: String
    var caption: String
    var likesNumber: String
    var commentsNumber: String
    var timeAndLocation: String
    var contentImage: String?
}

let allUsersData = [
    UserData(userName: "Sengly Sun", userProfile: "senglysun" , caption: "The thing that is really hard, and really amazing, is giving up on perfect and beginning the work of becoming yourself", likesNumber: "12 likes", commentsNumber: "23 comments",timeAndLocation: "Just now · Phnom penh" ,contentImage: nil),
    UserData(userName: "Cristaino Ronaldo", userProfile: "ronaldo", caption: "Victory in Turin...", likesNumber: "1.2m likes", commentsNumber: "2.5k comments", timeAndLocation: "12 mins · Turin Italy", contentImage: "ronaldoContent"),
    UserData(userName: "Mavin Sao", userProfile: "mavin" , caption: "ខ្ញុំឯការណាស់ ត្រូវការអ្នកមើលថែ", likesNumber: "123 likes", commentsNumber: "1 comments",timeAndLocation: "45 mins · prey veng",contentImage: nil),
    UserData(userName: "Chippy Sirin Preediyanon", userProfile: "chippy", caption: "Spider-man kiss 🤪", likesNumber: "3.5k likes", commentsNumber: "205 comments", timeAndLocation: "2 hours · Bangkok", contentImage: "chippyContent")
]
