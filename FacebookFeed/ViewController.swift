//
//  ViewController.swift
//  FacebookFeed
//
//  Created by Sengly Sun on 12/3/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class ViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = UIColor(red:0.26, green:0.40, blue:0.70, alpha:1.0)
        
        let homeTabbar = HomeTableViewController()
        homeTabbar.tabBarItem = UITabBarItem(tabBarSystemItem: .mostRecent, tag: 0)
        
        let otherTabBar1 = EmptyViewController()
        otherTabBar1.tabBarItem = UITabBarItem(tabBarSystemItem: .featured, tag: 1)
        
        let otherTabBar2 = EmptyViewController()
        otherTabBar2.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 2)
        
        let otherTabBar3 = EmptyViewController()
        otherTabBar3.tabBarItem = UITabBarItem(tabBarSystemItem: .mostViewed, tag: 3)
        
        let otherTabBar4 = EmptyViewController()
        otherTabBar4.tabBarItem = UITabBarItem(tabBarSystemItem: .history, tag: 4)
        
        viewControllers = [homeTabbar,otherTabBar1,otherTabBar2,otherTabBar3,otherTabBar4]
    }


}

