//
//  HomeTableViewController.swift
//  FacebookFeed
//
//  Created by Sengly Sun on 12/3/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class HomeTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "CaptionPostTableViewCell", bundle: nil), forCellReuseIdentifier: "captionCell")
        tableView.register(UINib(nibName: "ImagePostTableViewCell", bundle: nil), forCellReuseIdentifier: "imageCell")
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if allUsersData[indexPath.row].contentImage == nil{
            let cell = tableView.dequeueReusableCell(withIdentifier: "captionCell", for: indexPath) as! CaptionPostTableViewCell
            cell.userNameLabel.text = allUsersData[indexPath.row].userName
            cell.captionLabel.text = allUsersData[indexPath.row].caption
            cell.commentsNumberLabel.text = allUsersData[indexPath.row].commentsNumber
            cell.likesNumberLabel.text = allUsersData[indexPath.row].likesNumber
            cell.userProfile.image = UIImage(named: allUsersData[indexPath.row].userProfile)
            cell.relatedImage.image = UIImage(named: allUsersData[indexPath.row].userProfile)
            cell.timeAndLocationLabel.text = allUsersData[indexPath.row].timeAndLocation
        
            return cell
        
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "imageCell", for: indexPath) as! ImagePostTableViewCell
            cell.userName.text = allUsersData[indexPath.row].userName
            cell.caption.text = allUsersData[indexPath.row].caption
            cell.commentsNumber.text = allUsersData[indexPath.row].commentsNumber
            cell.likesNumber.text = allUsersData[indexPath.row].likesNumber
            cell.userProfile.image = UIImage(named: allUsersData[indexPath.row].userProfile)
            cell.relateImage.image = UIImage(named: allUsersData[indexPath.row].userProfile)
            cell.timeAndLocation.text = allUsersData[indexPath.row].timeAndLocation
            cell.contentImage.image = UIImage(named:allUsersData[indexPath.row].contentImage!)
            
            return cell
            
        }
        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        500
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return allUsersData.count
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
