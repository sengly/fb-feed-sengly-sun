//
//  CaptionPostTableViewCell.swift
//  FacebookFeed
//
//  Created by Sengly Sun on 12/3/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit
extension UIImageView {

   func makeRounded() {
    let radius = self.frame.width / 2
    self.layer.cornerRadius = radius
    self.layer.masksToBounds = true
    self.clipsToBounds = true
    self.contentMode = .scaleAspectFill
   }
    
}

extension UILabel{
    func makeCaptionFontFit(){
        self.adjustsFontSizeToFitWidth = true
        self.numberOfLines = 3
        self.minimumScaleFactor = 0.5
    }
}

extension UITextField{
    
    func makeCommentShape(){
        self.layer.cornerRadius = 15
        self.layer.borderWidth = 1
        self.borderStyle = .none
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
    }
    
}

class CaptionPostTableViewCell: UITableViewCell {

   
    
    @IBOutlet weak var userProfile: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeAndLocationLabel: UILabel!
    @IBOutlet weak var relatedImage: UIImageView!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var commentsNumberLabel: UILabel!
    @IBOutlet weak var likesNumberLabel: UILabel!
    @IBOutlet weak var commentTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userProfile.makeRounded()
        relatedImage.makeRounded()
        commentTextField.makeCommentShape()
        captionLabel.makeCaptionFontFit()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


