//
//  ImagePostTableViewCell.swift
//  FacebookFeed
//
//  Created by Sengly Sun on 12/3/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class ImagePostTableViewCell: UITableViewCell {

    @IBOutlet var commentTextField: UITextField!
    @IBOutlet var relateImage: UIImageView!
    @IBOutlet var commentsNumber: UILabel!
    @IBOutlet var likesNumber: UILabel!
    @IBOutlet var contentImage: UIImageView!
    @IBOutlet var caption: UILabel!
    @IBOutlet var timeAndLocation: UILabel!
    @IBOutlet var userName: UILabel!
    @IBOutlet var userProfile: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        userProfile.makeRounded()
        commentTextField.makeCommentShape()
        relateImage.makeRounded()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        // Configure the view for the selected state
    }
    
}
